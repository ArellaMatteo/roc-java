package org.rocstreaming.roctoolkit;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Collect open {@link NativeObjectReference} for avoid beeing garbage collected.
 *
 * @param <T>       the type of {@link NativeObjectReference} to be collected.
 */
class ReferenceCollector<T extends NativeObjectReference> implements Iterable<T> {

    /**
     * References map indexed by their native pointers.
     */
    private final Map<Long, T> references;

    /**
     * Create a new <code>ReferenceCollector</code>.
     */
    ReferenceCollector() {
        references = new ConcurrentHashMap<>();
    }

    /**
     * Add a new reference to the collection.
     *
     * @param reference         reference to add.
     */
    void add(final T reference) {
        references.put(reference.getPtr(), reference);
    }

    /**
     * Remove a reference from the collection.
     *
     * @param reference         reference to remove.
     */
    void remove(final T reference) {
        references.remove(reference.getPtr());
    }

    /**
     * Get the number of referencies inside the collection.
     *
     * @return      number of referencies.
     */
    int size() {
        return references.size();
    }

    /**
     * Iterator over <code>ReferenceCollector</code> iterating referencies with dependencies
     * ({@link NativeObjectReference#dependsOn}) firstly and secondly the referencies without
     * dependencies.
     */
    class ReferenceCollectorIterator implements Iterator<T> {

        /**
         * Iterator over referencies with dependencies.
         */
        private Iterator<T> depending;

        /**
         * Iterator over referencies without dependencies.
         */
        private Iterator<T> independing;

        /**
         * Create a new <code>ReferenceCollectorIterator</code>.
         */
        ReferenceCollectorIterator() {
            depending = references.values().parallelStream()
                            .filter(r -> r.getDependsOn().isPresent())
                            .iterator();
            independing = references.values().parallelStream()
                            .filter(r -> !r.getDependsOn().isPresent())
                            .iterator();
        }

        @Override
        public boolean hasNext() {
            return depending.hasNext() || independing.hasNext();
        }

        @Override
        public T next() {
            if (depending.hasNext()) return depending.next();
            if (independing.hasNext()) return independing.next();
            return null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ReferenceCollectorIterator();
    }
}