#!/usr/bin/env bash
set -euxo pipefail

docker pull rocproject/java-android:jdk$JAVA_VERSION
